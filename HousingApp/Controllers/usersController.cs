﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using HousingApp.Models;

namespace HousingApp.Controllers
{
    public class usersController : Controller
    {
        private HousingAppEntities db = new HousingAppEntities();
        
        //GET: users/
        public ActionResult Index()
        {
            return View();
        }
            //GET: users/login
        public ActionResult Login(user objUser)
        {
            var users = db.enquiries.Include(e => e.property).Include(e => e.user).Include(e => e.user1);
  
            if (ModelState.IsValid)
            {
                using (db)
                {
                    var obj = db.users.Where(a => a.username.Equals(objUser.username) && a.auth.Equals(objUser.auth)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserID"] = obj.users_id.ToString();
                        Session["Username"] = obj.username.ToString();
                        Session["active"] = 1;
                        return RedirectToAction("UserDashBoard");
                    }
                }
            }
            return View(objUser);

        }

        public ActionResult Logout(user objUser)
        {      
            if (objUser != null)
            {
                Session["UserID"] = " ";
                Session["Username"] = " ";
                Session["active"] = 0;
                return RedirectToAction("UserLogout");
            }
                return View(objUser);
        }
        public ActionResult UserLogout()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("index", "properties");
            }
            else
            {
                return RedirectToAction("login", "users");
            }
        }
        public ActionResult UserDashBoard()
        {
            if (Session["UserID"] != null)
            {
                return RedirectToAction("index", "properties");
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        // GET: users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "users_id,username,auth,active")] user u)
        {
           /* String un = u.username;
            String p = u.auth;
            p = un + p;
            un = createSha256Hash(un);
            p = createSha256Hash(p);
            u.username = un;
            u.auth = p;*/
                if (ModelState.IsValid)
                {
                    db.users.Add(u);
                    db.SaveChanges();
                return RedirectToAction("Login", "users");
            }
            return View();

        }

        
        
        static string createSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        // GET: users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "users_id,username,auth,active")] user user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            user user = db.users.Find(id);
            db.users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
