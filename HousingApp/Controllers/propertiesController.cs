﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HousingApp.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace HousingApp.Controllers
{
    public class propertiesController : Controller
    {
        private HousingAppEntities db = new HousingAppEntities();

        // GET: properties/index
        public ActionResult Index()
        {
            var properties = db.properties.Include(p => p.user);
            return View(properties.ToList());
        }

        // GET: properties/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            property property = db.properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }
            return View(property);
        }

        public ActionResult inquire(property p)
        {
            Session["toID"] = p.users_id.ToString();
            Session["regarding"] = p.property_id.ToString();
            return RedirectToAction("create", "enquiries");
        }

        // GET: properties/Create
        public ActionResult Create()
        {
            ViewBag.users_id = new SelectList(db.users, "users_id", "username");
            return View();
        }

        // POST: properties/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "property_id,address_line1,address_line2,town_city,users_id")] property property)
        {
            if (ModelState.IsValid)
            {
                db.properties.Add(property);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.users_id = new SelectList(db.users, "users_id", "username", property.users_id);
            return View(property);
        }

        // GET: properties/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            property property = db.properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }
            ViewBag.users_id = new SelectList(db.users, "users_id", "username", property.users_id);
            return View(property);
        }

        // POST: properties/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "property_id,address_line1,address_line2,town_city,users_id")] property property)
        {
            if (ModelState.IsValid)
            {
                db.Entry(property).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.users_id = new SelectList(db.users, "users_id", "username", property.users_id);
            return View(property);
        }

        // GET: properties/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            property property = db.properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }
            return View(property);
        }

        // POST: properties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            property property = db.properties.Find(id);
            db.properties.Remove(property);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
