﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HousingApp.Models;

namespace HousingApp.Controllers
{
    public class enquiriesController : Controller
    {
        private HousingAppEntities db = new HousingAppEntities();

        // GET: enquiries
        public ActionResult Index()
        {
            var enquiries = db.enquiries.Include(e => e.property).Include(e => e.user).Include(e => e.user1);
            return View(enquiries.ToList());
        }

        // GET: enquiries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            enquiry enquiry = db.enquiries.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }
            return View(enquiry);
        }

        // GET: enquiries/Create
        public ActionResult Create()
        {
            ViewBag.property_id = new SelectList(db.properties, "property_id", "address_line1");
            ViewBag.users_to = new SelectList(db.users, "users_id", "username");
            ViewBag.users_from = new SelectList(db.users, "users_id", "username");
            return View();
        }

        // POST: enquiries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "enquiry_id,property_id,users_from,users_to,content")] enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                db.enquiries.Add(enquiry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.property_id = new SelectList(db.properties, "property_id", "address_line1", enquiry.property_id);
            ViewBag.users_to = new SelectList(db.users, "users_id", "username", enquiry.users_to);
            ViewBag.users_from = new SelectList(db.users, "users_id", "username", enquiry.users_from);
            return RedirectToAction("index", "properties");
        }

        // GET: enquiries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            enquiry enquiry = db.enquiries.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }
            ViewBag.property_id = new SelectList(db.properties, "property_id", "address_line1", enquiry.property_id);
            ViewBag.users_to = new SelectList(db.users, "users_id", "username", enquiry.users_to);
            ViewBag.users_from = new SelectList(db.users, "users_id", "username", enquiry.users_from);
            return View(enquiry);
        }

        // POST: enquiries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "enquiry_id,property_id,users_from,users_to,content")] enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(enquiry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.property_id = new SelectList(db.properties, "property_id", "address_line1", enquiry.property_id);
            ViewBag.users_to = new SelectList(db.users, "users_id", "username", enquiry.users_to);
            ViewBag.users_from = new SelectList(db.users, "users_id", "username", enquiry.users_from);
            return View(enquiry);
        }

        // GET: enquiries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            enquiry enquiry = db.enquiries.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }
            return View(enquiry);
        }

        // POST: enquiries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            enquiry enquiry = db.enquiries.Find(id);
            db.enquiries.Remove(enquiry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
