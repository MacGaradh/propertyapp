﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HousingApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
              name: "Register",
              url: "register",
              defaults: new { controller = "users", action = "create", id = UrlParameter.Optional }
          );
            routes.MapRoute(
             name: "Login",
             url: "login",
             defaults: new { controller = "users", action = "Login", id = UrlParameter.Optional }
         );
            routes.MapRoute(
             name: "Logout",
             url: "users/logout",
             defaults: new { controller = "users", action = "Logout", id = UrlParameter.Optional }
         );
            routes.MapRoute(
            name: "Property",
            url: "properties/index",
            defaults: new { controller = "properties", action = "index", id = UrlParameter.Optional }
         );
            routes.MapRoute(
            name: "Enquiry",
            url: "enquiries/index",
            defaults: new { controller = "enquiries", action = "index", id = UrlParameter.Optional }
         );
            routes.MapRoute(
            name: "Inquire",
            url: "enquiries/create/",
            defaults: new { controller = "enquiries", action = "create", id = UrlParameter.Optional }
         );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "users", action = "create", id = UrlParameter.Optional }
            );
          
           
        }
    }
}
